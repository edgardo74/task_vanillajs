// After fetching we store the data in a global variable so we can access this data later when toggling occurs.
let DATA = null
fetch("https://humanit.se/wp-json/wp/v2/whitepaper/") // Call the fetch function passing the url of the API as a parameter.
.then(response => response.json())
.then(data => {
    let main_container = document.getElementById('container') 
    for( let i = 0; i < data.length; i++){
        let truncated_meta = truncateText(data[i].content.rendered) 
        data[i].is_truncated = truncated_meta.is_truncated
        data[i].truncated_text = truncated_meta.truncated_text // We add these two properties to the comment object.
        handleTruncationUI(data[i], main_container)
    }
    DATA = data 
})
.catch(function(error) {
    console.log(error)
});

// Function truncateText(text, noOfWords=50 ), this tokenizes the text and returns a tuple which is essentially a meta-data letting us know if the whitepaper exceeded 50 words.
// Here we use some regular expresions used.
function truncateText( text, noOfWords = 50 ){
    text = text.replace(/^<p[^>]*>/,"") // Remove the starting P tag.
    text = text.replace(/<\/p[^>]*>$/ ,"") // Remove the ending P tag as well.

    const words = text.split(/\s+/g) // Regular expression to split text based on whitespace charaters(space,tab, newline etc).
    let truncated_text = text 
    let is_truncated = false

    if (words.length > noOfWords){
        // We also removed the <p> tag from the first word.
        is_truncated = true
        first_n_words = words.slice(0, noOfWords) // Here we get an array of N words.
        truncated_text = first_n_words.join(" ") // Then, we convert the array to a string.
        truncated_text = truncated_text.replace(/<.*/, ""); // Here we remove all the tags in the truncated text
        // as it can lead to invalid html structure.
    }
    return { 'is_truncated' : is_truncated, 'truncated_text':  truncated_text} 
}

// This function getCommentById(id) returns the element from data having specific id.
function getCommentById( id ){
    for( let i = 0 ; i< DATA.length; i++ ){
        if(DATA[i].id==id)
            return DATA[i] 
        // else shouldn't happen, we are using the keys from the id of whitepapers in DATA.
    }
    return null
}

// This function toggleText(id) handles the toggling effect, it adds and removes classes and also alters text on each whitepaper (toggle). Here we pass the ID of the whitepaper to it that has the text we want to toggle. First it'll select the Div that contains the content by matching data-attribute. Then it'll select its children, the first children is the Heading, the second is the Body, the third is the container with ... or .and the fourth children is the button. After that, there is basic if-else condition for a class of show on the toggle button.
function toggleText(id){
    let element = document.querySelector('[data-id="'+id+'"]')
    let body_span = element.children[1]
    let toggle_span = element.children[2]
    let toggle_button = element.children[3]
    let data_item = getCommentById(id)

    console.log(element)
    if( toggle_button.classList.contains('show')){
        toggle_button.classList.remove('show')
        toggle_span.innerHTML = " ..."
        body_span.innerHTML = data_item.truncated_text
        toggle_button.innerHTML = "READ MORE"

    }else{
        toggle_span.innerHTML = ""
        body_span.innerHTML = data_item.content.rendered
        toggle_button.classList.add('show')
        toggle_button.innerHTML = "READ LESS"
    }
}

// This function handleTruncationUI(data_item, parent) handles how you want to deal with displaying the item in the parent container
function handleTruncationUI(data_item, parent){ // This depends on how u want to structure your UI; but its enough for the general idea
    let item = document.createElement('div')
    item.className = "whitepaperBox"
    item.setAttribute('data-id', data_item.id);
    item.innerHTML = '<h3>'+data_item.title.rendered+'</h3>'
    if(data_item.is_truncated){
        let extra_span =  '<span class="toggle">...</span>'
        item.innerHTML += '<span class="content">' + data_item.truncated_text +'</span>' + extra_span;
        item.innerHTML += '<button class="btnReadMore" onclick="toggleText('+data_item.id+');">READ MORE</button>'
    }else{
        item.innerHTML = data_item.body
    }

    parent.appendChild(item)
}


// UNIT TEST
///////////////////

// The following unit test is taking two things:
// 1. Input(text and n_words parameter). 
// 2. Expected Output.

// Here you have to know the expected output corresponding to the input, the expected output is like a hard-coded value of the results which you know beforehand are correct.
// - The Unit test then calls the Tokenize_text to get the actual_output.
// - Then, it'll compare the values of expected_output with the actual_output.
// If they match then test prints to the console "Passed" else "Failed".

//////////////////

function truncateTextTest(input, expected_output) {
    actual_output = truncateText(input.text, input.tokens) // default N is 50
    if ((actual_output.is_truncated === expected_output.is_truncated) & (actual_output.truncated_text === expected_output.truncated_text))
        console.log("TEST PASSED")
    else
        console.log("TEST FAILED")
}

truncateTextTest({ "text": "Test with 4 words", tokens: 10 }, { "is_truncated": false, "truncated_text": "Test with 4 words" })
truncateTextTest({ "text": "Test with 12 words 5 6 7 8 9 10 11 12", tokens: 10 }, { "is_truncated": true, "truncated_text": "Test with 12 words 5 6 7 8 9 10" })
truncateTextTest({ "text": "Test with 4 words", tokens: 2 }, { "is_truncated": true, "truncated_text": "Test with" })
truncateTextTest({ "text": "", tokens: 10 }, { "is_truncated": false, "truncated_text": "" })
