## Code Test - Edgardo Pinto-Escalier

Task assignment/Code test for Human IT.

---

## Task description

The task consisted to fetch data from a given link (REST API), format the data and present it in a readable way. The text from the data should be truncated.

1. The task was completed using **Vanilla JavaScript**, no frameworks were used.
2. The CSS styles are pure **CSS ONLY**, no css frameworks were used for this project either.
3. The project has a **Unit Test** that checks and passes a test for one function.
4. The application shows the title and content of 10 whitepapers. No images or .pdf files were found on the JSON.
5. The **main.js** file has relevant comments about the code as well as what every function does.

---

## Scope

This application is fully responsive, it has been tested with Chrome, Safari and Firefox as well as IE11/Edge+ and works well with the latest mobile versions of Chrome and Safari.
The application can be seen lihere here: [Task VanillaJS](https://task-vanillajs.netlify.com/)

---

## How to install and use this application

To use this application please follow these steps:

1. Clone the repository to your local environment.
2. Open the **index.html** file to see the project.
3. This project requires no installation of any packages.
